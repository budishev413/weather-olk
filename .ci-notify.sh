#!/usr/bin/env bash

curl -s -X POST https://api.telegram.org/bot"$2"/sendMessage \
                -d chat_id="$3" \
                -d disable_web_page_preview=1 \
                -d parse_mode=markdown \
                -d text="*GitLab*%0A*Status*: $1%0A*Pipeline*: ${CI_PIPELINE_URL}%0A*INFO*: $4"
                > /dev/null

