from datetime import timedelta
from smtpd import DebuggingServer
import settings
import telebot
import logging
import mysql.connector
from mysql.connector import Error
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import pandas as pd
from sqlalchemy import create_engine
import os

logging.basicConfig(level=logging.INFO)

log = logging.getLogger(__name__)

console = logging.StreamHandler()
file = logging.FileHandler('logs/bot.log', mode='a', encoding='utf-8')
console.setLevel(logging.ERROR)
file.setLevel(logging.INFO)

fileFormat = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s',
                               '%d-%b-%y %H:%M:%S')

file.setFormatter(fileFormat)

log.addHandler(console)
log.addHandler(file)

bot = telebot.TeleBot(os.environ['BOT_TOKEN'])

dbhost = os.environ['DB_HOST']
dbuser = os.environ['DB_USER']
dbpass = os.environ['DB_PASS']
dbname = os.environ['DB_NAME']

def utc_to_local(utc_dt):
    return utc_dt + timedelta(hours=settings.TIMEDELTA)


def getData(message):
    try:
        db = mysql.connector.connect(
            host=dbhost,  
            user=dbuser,
            password=dbpass,
            database=dbname
        )
        cursor = db.cursor()
        cursor.execute("SELECT `t4`, `time` FROM `curdata`")
        res = cursor.fetchall()

        for data in res:
            getData.outside = data[0]
            getData.time = data[1]

    except Error as e:
        err_msg = ("Error reading data from MySQL table", e)
        log.error(err_msg)
        bot.send_message(message.chat.id, "MySQL error:\n{err.msg}")

    finally:
        if (db.is_connected()):
            db.close()
            cursor.close()


@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    bot.send_message(message.chat.id, "Type /temp")
    log.info(f"Cmd: '{message.text}' from: {message.chat.id}")


@bot.message_handler(commands=['temp'])
def send_temperatrue(message):
    getData(message)
    
    bot.send_message(message.chat.id, f'Температура: {getData.outside:.1f}℃\nВремя: {utc_to_local(getData.time).strftime("%H:%M %d.%m.%Y")}')
    log.info(f"Cmd: '{message.text}' from: {message.chat.id} {message.from_user.username}")


@bot.message_handler(commands=['last12'])
def send_last12_graph(message):
    db = create_engine(
    f'mysql+pymysql://{dbuser}:{dbpass}@{dbhost}/{dbname}')

    query = "SELECT `temp`, CONVERT_TZ(`date`,'+00:00','+06:00') AS date FROM `data` WHERE date > DATE_ADD(NOW(), INTERVAL -12 HOUR)"

    df = pd.read_sql(query, db, index_col=["date"])
    fig, ax = plt.subplots(constrained_layout=True)
    locator = mdates.AutoDateLocator(minticks=3, maxticks=7)
    formatter = mdates.ConciseDateFormatter(locator)
    ax.xaxis.set_major_locator(locator)
    ax.xaxis.set_major_formatter(formatter)
    df.plot(ax=ax)
    ax.set_xlabel("Время")
    ax.set_ylabel("℃")
    ax.set_title("Температура за последние 12 часов")
    ax.get_legend().remove()
    ax.grid()

    fig.savefig("last_12h.png")
    pic = open("last_12h.png", "rb")

    bot.send_photo(message.chat.id, pic)
    log.info(f"Cmd: '{message.text}' from: {message.chat.id} {message.from_user.username}")

@bot.message_handler(func=lambda message: True)
def echo_all(message):
    bot.reply_to(message, message.text)
    log.info(f"Msg: '{message.text}' from: {message.chat.id}")


bot.infinity_polling()
